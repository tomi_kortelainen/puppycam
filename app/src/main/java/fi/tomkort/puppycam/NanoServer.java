package fi.tomkort.puppycam;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by Tomi on 18.2.2015.
 */
public class NanoServer extends NanoHTTPD {

    public static final String
            MIME_PLAINTEXT = "text/plain",
            MIME_HTML = "text/html",
            MIME_JS = "application/javascript",
            MIME_CSS = "text/css",
            MIME_PNG = "image/png",
            MIME_JPG = "image/jpeg",
            MIME_DEFAULT_BINARY = "application/octet-stream",
            MIME_XML = "text/xml";

    String password = "";

    Context ctx;

    NanoServer(Context ctx, int port) throws IOException {
        super(port);
        this.ctx = ctx;
    }

    @Override
    public Response serve(IHTTPSession session) {

        String uri = session.getUri();
        CookieHandler cookies = session.getCookies();

        if (uri.equals("/login")) {
            Map<String, String> files = new HashMap<String, String>();

            try {
                session.parseBody(files);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ResponseException e) {
                e.printStackTrace();
            }

            cookies.set("passw", session.getParms().get("password"), 100);
            Response response = new Response(Response.Status.REDIRECT, NanoHTTPD.MIME_HTML, "Jea!");
            response.addHeader("Location", "/index.html");
            return response;

        }

        if (cookies.read("passw") == null) {
            return serveFile("/login.html", MIME_HTML);
        } else if (!cookies.read("passw").equals(password)) {
            return serveFile("/login.html", MIME_HTML);
        }

        if (uri.contains(".jpg")) {
            return serveImage(uri, MIME_JPG);
        }
        return serveFile("/index.html", MIME_HTML);
    }

    public Response serveImage(String uri, String mime) {

        InputStream is = null;
        try {
            PackageManager pm = ctx.getPackageManager();
            String path = ctx.getPackageName();

            try {
                PackageInfo pi = pm.getPackageInfo(path, 0);
                path = pi.applicationInfo.dataDir;

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            is = new FileInputStream(path + uri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new NanoHTTPD.Response(Response.Status.OK, mime, is);
    }

    public Response serveFile(String uri, String mime) {

        InputStream is = null;
        try {
            is = ctx.getAssets().open(uri.substring(1));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new NanoHTTPD.Response(Response.Status.OK, mime, is);
    }

    public void setPassword(String passwrd) {
        password = passwrd;
    }
}
