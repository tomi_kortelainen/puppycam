package fi.tomkort.puppycam;

import android.app.ActionBar;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceView;
import android.view.ViewGroup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Tomi on 28.2.2015.
 */
public class CameraHandler {

    DummyPreview preview;
    Context ctx;
    Camera camera;
    Handler handler;
    int delay = 3000;

    public CameraHandler(Context ctx)
    {
        this.ctx = ctx;

        preview = new DummyPreview(ctx, new SurfaceView(ctx));
        preview.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        preview.setKeepScreenOn(true);

        handler = new Handler();

        initCamera();

    }

    private void initCamera() {
        if (camera != null)
            camera = null;

        try {
            camera = Camera.open(0);
            camera.startPreview();
            preview.setCamera(camera);
            pictureTaker.run();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        camera.release();
        camera = null;
        handler.removeCallbacks(pictureTaker);
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public void takePicture() {
        if (camera == null)
            Log.e("WAT", "EIOOOOOOO!!!");
        //camera.startPreview();
        camera.takePicture(shutterCallback, rawCallback, pictureCallback);
        //camera.stopPreview();
    }

    private class SavePicture extends AsyncTask<byte[], Void, Void> {
        @Override
        protected Void doInBackground(byte[]... data) {
            FileOutputStream outStream = null;

            try {
                PackageManager pm = ctx.getPackageManager();
                String s = ctx.getPackageName();

                PackageInfo pi = pm.getPackageInfo(s, 0);
                s = pi.applicationInfo.dataDir;

                File dir = new File(s);

                String fileName = "image.jpg";
                File outFile = new File(dir, fileName);
                outFile.createNewFile();

                outStream = new FileOutputStream(outFile);
                outStream.write(data[0]);
                outStream.flush();
                outStream.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            } finally {
            }
            return null;
        }
    }

    Runnable pictureTaker = new Runnable() {
        @Override
        public void run() {
            takePicture();
            handler.postDelayed(pictureTaker, delay);
        }
    };

    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {

        }
    };

    Camera.PictureCallback rawCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

        }
    };

    Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            new SavePicture().execute(data);
        }
    };
}
