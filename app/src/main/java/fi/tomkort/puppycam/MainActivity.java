package fi.tomkort.puppycam;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;


public class MainActivity extends Activity {

    CameraHandler cameraHandler = null;
    Button btnStartServer;
    TextView txtIP;
    EditText editPort;
    EditText editPassword;

    private NanoServer server;

    public static String getLocalIp() {

        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStartServer = (Button) findViewById(R.id.btnStart);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editPort = (EditText) findViewById(R.id.editPort);
        txtIP = (TextView) findViewById(R.id.txtIP);
        txtIP.setText(getLocalIp());

        btnStartServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (server == null) {
                        server = new NanoServer(getApplicationContext(), Integer.parseInt(editPort.getText().toString()));
                        server.setPassword(editPassword.getText().toString());
                        server.start();
                        btnStartServer.setText(R.string.btnStop);
                    } else {
                        server.stop();
                        server = null;
                        btnStartServer.setText(R.string.btnStart);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        cameraHandler = new CameraHandler(getApplicationContext());


    }

    @Override
    protected void onPause() {
        super.onPause();

        if (server != null) {
            server.stop();
            server = null;
        }

        cameraHandler.close();
        cameraHandler = null;
    }

}
